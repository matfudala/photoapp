insert into users (username, password, enabled, email)
values ('admin', '$2a$10$hUhRqwjpeL8VULyu3vUuuui5.GTTmvur7Yx53MaHzmxEoqvZ298Ua', true, 'admin@mail.com'),
       ('user', '$2a$10$FIYwIkSDIDfupIAfgHsDu.cwdIB5CSeoBTrvgR2N7JDUGJ3E4EF6y', true, 'user@mail.com');

insert into role (name)
values ('ROLE_ADMIN'),
       ('ROLE_USER');

insert into users_roles (user_id, role_id)
values (1, 1),
       (1, 2),
       (2, 2);