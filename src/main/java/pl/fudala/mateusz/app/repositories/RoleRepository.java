package pl.fudala.mateusz.app.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.fudala.mateusz.app.model.user.Role;

@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {
    Role findByName(String roleUser);
}
