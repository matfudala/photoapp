package pl.fudala.mateusz.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import pl.fudala.mateusz.app.model.data.Photo;
import pl.fudala.mateusz.app.model.user.User;
import pl.fudala.mateusz.app.repositories.UserRepository;

import java.util.List;

@SpringBootApplication
public class PhotoAppApplication implements CommandLineRunner {

    private final UserRepository userRepository;

    @Autowired
    public PhotoAppApplication(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public static void main(String[] args) {
        SpringApplication.run(PhotoAppApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = userRepository.findByUsername("user");
        List<Photo> photos = user.getLinksToPhotos();
        photos.add(new Photo("https://source.unsplash.com/pWkk7iiCoDM/400x300"));
        photos.add(new Photo("https://source.unsplash.com/aob0ukAYfuI/400x300"));
        photos.add(new Photo("https://source.unsplash.com/EUfxH-pze7s/400x300"));
        userRepository.save(user);
    }
}
