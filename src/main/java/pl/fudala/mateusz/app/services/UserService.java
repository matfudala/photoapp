package pl.fudala.mateusz.app.services;

import org.springframework.security.core.userdetails.UserDetailsService;
import pl.fudala.mateusz.app.dto.user.UserDto;
import pl.fudala.mateusz.app.model.user.User;

public interface UserService extends UserDetailsService {
    User findByUsername(String username);
    User save(UserDto userDto);
}
