package pl.fudala.mateusz.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.fudala.mateusz.app.model.data.Photo;
import pl.fudala.mateusz.app.model.user.Role;
import pl.fudala.mateusz.app.model.user.User;
import pl.fudala.mateusz.app.repositories.RoleRepository;
import pl.fudala.mateusz.app.repositories.UserRepository;

import java.security.Principal;

import static pl.fudala.mateusz.app.controllers.HomeController.REDIRECT;

@Controller
@RequestMapping("/gallery")
public class GalleryController {

    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private static final String USERNAME = "username";
    private static final String ROLE_ADMIN = "ROLE_ADMIN";

    @Autowired
    public GalleryController(UserRepository userRepository, RoleRepository roleRepository) {
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
    }

    @GetMapping
    public String homeController(Principal principal, Model model) {

        User loggedUser = userRepository.findByUsername(principal.getName());
        Role roleAdmin = roleRepository.findByName(ROLE_ADMIN);

        if (loggedUser.getRoles().contains(roleAdmin)) {
            return REDIRECT + "admin";
        } else {
            model.addAttribute("photos", loggedUser.getLinksToPhotos());
            model.addAttribute(USERNAME, principal.getName());
            return "gallery";
        }
    }

    @GetMapping("/{username}")
    public String userGallery(@PathVariable String username, Model model) {
        model.addAttribute("photos", userRepository.findByUsername(username).getLinksToPhotos());
        model.addAttribute(USERNAME, username);
        return "gallery";
    }

    @GetMapping("/{username}/add")
    public String addPhotoForm(@PathVariable String username, Model model) {
        model.addAttribute("photo", new Photo());
        model.addAttribute(USERNAME, username);
        return "add-photo";
    }

    @PostMapping("/{username}/add")
    public String addPhoto(@ModelAttribute Photo photo, @PathVariable String username) {
        User user = userRepository.findByUsername(username);
        user.getLinksToPhotos().add(photo);
        userRepository.save(user);
        return REDIRECT + "/gallery/" + username;
    }
}
