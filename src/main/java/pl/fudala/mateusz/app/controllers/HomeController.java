package pl.fudala.mateusz.app.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.fudala.mateusz.app.dto.user.UserDto;
import pl.fudala.mateusz.app.model.user.User;
import pl.fudala.mateusz.app.services.UserService;

@Controller
@RequestMapping("/")
public class HomeController {

    private final UserService userService;
    public static final String REDIRECT = "redirect:";

    @Autowired
    public HomeController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = {"/login", "/"})
    public String loginPage(Model model) {
        model.addAttribute("user", new UserDto());
        return "login";
    }

    @PostMapping("/register")
    public String registerUser(@ModelAttribute("user") UserDto user) {
        User existing = userService.findByUsername(user.getUsername());

        if (existing != null) {
            return REDIRECT + "login?exists";
        }
        userService.save(user);
        return "login";
    }
}
